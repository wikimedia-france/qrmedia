��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �     �     �     �     �     	           2     G     X     v     �     �     �     �     �     �  4         5     M     `     u  '   �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-24 19:17+0100
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.3
 (ver/ocultar) Parámetros avanzados Artículo en Wikipedia Cursos en Wikiversidad Datos en Wikidata Datos con Reasonator Desarrollado por Archivos en Wikimedia Commons Repósitorio Framagit Guías en Wikivoyage Página principal Manuales en Wikibooks Nuevo QRCode Actualidades en Wikinews Tag del proyecto Generador de códigos QR hacia páginas de Wikimedia Lugar de visualización Citas en Wikiquote Textos en Wikisource Portada QRMedia El identificador Wikidata es requerido. Elemento Wikidata 
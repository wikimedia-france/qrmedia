��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �     �     �     �     �               0     D     Q     m     ~     �     �  	   �     �     �  $   �               '     =     N     b            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-24 19:39+0100
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.3
 (visa/dölja) Avancerade parametrar Artikel på Wikipedia Kurser om Wikiversity Data på Wikidata Data med Reasonator Utvecklad av Filer på Wikimedia Commons Framagit-arkivet Guide på Wikivoyage Hemsida Manualer på Wikibooks Ny QR-kod Nyheter på Wikinews Projektetikett QR-kodgenerator till Wikimedia-sidor Visningsplats Citat på Wikiquote Texter på Wikisource QRMedia-portalen Wikidata-ID krävs. Wikidata-objekt 
��          �      �           	          )     >     U     f     {     �     �     �     �     �     �  
   �            $        D     X     l          �  ,  �     �     �     �          '     9     N     `     o     �     �     �     �     �             1   %     W     o     �  ,   �     �            
                                                                   	                         (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Display place Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-28 14:30+0100
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: Emma Vadillo
Language-Team: 
X-Generator: Poedit 2.3
 (mostrar/esconder) Configurações avançadas Artigo na Wikipedia Cursos na Wikiversity Dados na Wikidata Dados com Reasonator Desenvolvido pela Visualização Ficheiros na Wikimedia Commons Repositório Framagit Guia de viagem na Wikivoyage Página principal Livro didático na Wikibooks Novo código QR Notícia na Wikinews Tag de projecto Gerador de códigos QR para páginas da Wikimedia Citações na Wikiquote Textos na Wikisource Portal QRMedia Q identificador d'iten Wikidata necessitado. Itens wikidata 
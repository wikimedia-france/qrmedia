��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �     �     �     �     �          %     8     M     \     z     �  
   �     �     �     �     �  '         (     E     Z     o     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-24 19:07+0100
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 (Anzeigen/Ausblenden) Erweiterte Parameter Artikel auf Wikipedia Kurse auf Wikiversity Daten auf Wikidata Daten mit Reasonator Entwickelt von Dateien auf Wikimedia Commons Framagit-Repository Reiseführer auf Wikivoyage Startseite Handbücher zu Wikibooks Neuer QRCode Nachrichten auf Wikinews Projekt-Tag QR-Code-Generator für Wikimedia-Seiten Position der QR-Code-Anzeige Zitate auf Wikiquote Texte auf Wikisource Das QRMedia-Portal Das Qid ist erforderlich. Wikidata-Element 
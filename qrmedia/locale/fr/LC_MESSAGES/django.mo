��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �    �     �     �     �     
     !     7     P     `          �     �     �     �     �     �  1   �     +     G     _     u  $   �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-24 19:05+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 (montrer/cacher) Paramètres avancés Article sur Wikipédia Cours sur Wikiversité Données sur Wikidata Données avec Reasonator Développé par Fichiers sur Wikimedia Commons Dépôt Framagit Guide sur Wikivoyage Accueil Manuels sur Wikibooks Nouveau QRCode Actualités sur Wikinews Tag de projet Générateur de QR codes vers des pages Wikimedia Lieu d'affichage du QR code Citations sur Wikiquote Textes sur Wikisource Portail QRMedia L’identifiant Wikidata est requis. Élément Wikidata 
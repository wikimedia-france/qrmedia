# Translation of QRmedia to Persian (فارسی)
# Exported from translatewiki.net
#
# Author: Beginneruser
# Author: Darafsh
# --
# Localization strings for QRMedia
# Copyright (C) 2020 Wikimédia France
# This file is distributed under the same license as the QRMedia package.
# Sylvain Boissel <sylvain.boissel@wikimedia.fr>, 2020.
#
msgid ""
msgstr ""
""
"PO-Revision-Date: 2023-04-27 11:17:59+0000\n"
"X-POT-Import-Date: 2021-05-26 12:33:35+0000\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: qrmedia/templates/qrmedia/base.html:10
msgid "QR Code generator to Wikimedia pages"
msgstr "تولیدکنندهٔ کد QR صفحه‌های ویکی‌مدیا"

#: qrmedia/templates/qrmedia/base.html:30
msgid "Homepage"
msgstr "صفحهٔ اصلی"

#: qrmedia/templates/qrmedia/base.html:55
msgid "Framagit repository"
msgstr "مخزن فراماگیت"

#: qrmedia/templates/qrmedia/base.html:60
msgid "Developed by"
msgstr "توسعه‌دهنده:"

#: qrmedia/templates/qrmedia/entry.html:4
msgid "The QRMedia Portal"
msgstr "درگاه QRMedia"

#: qrmedia/templates/qrmedia/entry.html:34
msgid "Article on Wikipedia"
msgstr "مقالهٔ ویکی‌پدیا"

#: qrmedia/templates/qrmedia/entry.html:41
msgid "News on Wikinews"
msgstr "خبر ویکی‌خبر"

#: qrmedia/templates/qrmedia/entry.html:48
msgid "Manuals on Wikibooks"
msgstr "راهنماهای ویکی‌کتاب"

#: qrmedia/templates/qrmedia/entry.html:55
msgid "Quotes on Wikiquote"
msgstr "گفتاوردهای ویکی‌گفتاورد"

#: qrmedia/templates/qrmedia/entry.html:62
msgid "Texts on Wikisource"
msgstr "متون ویکی‌نبشته"

#: qrmedia/templates/qrmedia/entry.html:72
msgid "Courses on Wikiversity"
msgstr "دوره‌های ویکی‌دانشگاه"

#: qrmedia/templates/qrmedia/entry.html:79
msgid "Guide on Wikivoyage"
msgstr "راهنمای ویکی‌سفر"

#: qrmedia/templates/qrmedia/entry.html:89
msgid "Files on Wikimedia Commons"
msgstr "پروندهٔ ویکی‌انبار"

#: qrmedia/templates/qrmedia/entry.html:104
msgid "Data on Wikidata"
msgstr "دادهٔ ویکی‌داده"

#: qrmedia/templates/qrmedia/entry.html:105
msgid "Data with Reasonator"
msgstr "Data with Reasonator"

#: qrmedia/templates/qrmedia/homepage.html:11
msgid "New QRCode"
msgstr "کد QR جدید"

#: qrmedia/templates/qrmedia/homepage.html:16
msgid "Wikidata item"
msgstr "آیتم ویکی‌داده"

#: qrmedia/templates/qrmedia/homepage.html:20
msgid "The Qid is required."
msgstr "شناسهٔ Q ضروری است."

#: qrmedia/templates/qrmedia/homepage.html:25
msgid "Advanced parameters"
msgstr "پارامترهای پیشرفته"

#: qrmedia/templates/qrmedia/homepage.html:25
msgid "(show/hide)"
msgstr "(نمایش/پنهان)"

#: qrmedia/templates/qrmedia/homepage.html:28
msgid "Project tag"
msgstr "برچسب پروژه"

#: qrmedia/templates/qrmedia/homepage.html:38
msgid "QR code display location"
msgstr "کد QR نمایش موقعیت"


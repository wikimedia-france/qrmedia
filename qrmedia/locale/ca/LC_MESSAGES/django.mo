��          �      |      �     �     �          &     =     N     c     p     �     �     �     �  
   �     �     �     �          &     :     M     b     p     �     �     �     �     �     �     	          6     J     ]     m  
   �     �     �     �     �     �     �  %        ,                                  	                          
                                     (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-24 19:26+0100
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.3
 (veure/amagar) Paràmetres avançats Article en Wikipedia Cursos en Wikiversidad Dadas en Wikidata Dadas amb Reasonator Desenvolupat per Arxius en Wikimedia commons Repositori Framagit Guia en Wikivoyage Pàgina d'inici Manuals en Wikibooks Nou QRCode Actualitats en Wikinews Tag d'el projecte Lloc de visualització Cites en Wikiquote Textos en Wikisource Portada QRMedia L'identificador Wikdiata es requerit. Element Wikidata 
from django.contrib import admin
from .models import Item, Panel, Project, Stat

admin.site.register(Item)
admin.site.register(Panel)
admin.site.register(Project)
admin.site.register(Stat)

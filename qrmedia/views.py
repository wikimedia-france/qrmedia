import json
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Article, Project
from .models import get_commons_files, get_works, get_panel_stats, get_project_stats
import time
from datetime import date, timedelta
import html


def index(request):
    projects = Project.objects.all()
    return render(request, "qrmedia/homepage.html", {"projects": projects})


def project_stats(request, project):
    data = {}
    data['projects'] = Project.objects.all()
    if project:
        data["selected_project"] = project

    return render(request, "qrmedia/stats-project.html", data)


def panel_stats(request, project, panel):
    data = {}
    data['project'] = project
    data['panel'] = panel
    return render(request, "qrmedia/stats-panel.html", data)


def fkp_redirect(request, qid, pj=""):
    fkp_url = "https://tools.wmflabs.org/portal/"
    return redirect(fkp_url + qid)


def get_item_data(request, qid):
    # Contains the logic for the two next views
    # Only difference is the type of render
    start = time.time()
    lang = request.GET.get("lang")
    if not lang:
        lang = request.LANGUAGE_CODE
    project = request.GET.get("pk_campaign")
    place = request.GET.get("pk_kwd")
    entry = Article(qid, lang)
    entry.get_live_wd_data()
    entry.get_wp_summary()
    data = entry.get_data()
    data["panel"] = {'project': project, 'place': place}
    data["loadtime"] = time.time() - start
    return data


def portal(request, qid):
    data = get_item_data(request, qid)
    return render(request, "qrmedia/entry.html", data)


def item_api(request, qid):
    data = get_item_data(request, qid)
    return HttpResponse(json.dumps(data), content_type="application/json")


def commons_files_api(request, category):
    category = html.escape(category)
    number = request.GET.get("number")
    thumb = request.GET.get("thumb")
    if not number:
        number = 6
    if not thumb:
        thumb = 80
    start = time.time()
    data = get_commons_files(category, number, thumb)
    data["commons_category"] = category
    data["loadtime"] = time.time() - start
    return HttpResponse(json.dumps(data), content_type="application/json")


def works_api(request, qid):
    start = time.time()
    limit = request.GET.get("limit")
    lang = request.GET.get("lang")
    if not lang:
        lang = request.LANGUAGE_CODE
    data = {"works": get_works(qid, lang, limit)}
    data["loadtime"] = time.time() - start
    return HttpResponse(json.dumps(data), content_type="application/json")


def panel_stats_api(request, panel_id):
    start = time.time()
    data = get_panel_stats(panel_id)
    data["loadtime"] = time.time() - start
    return HttpResponse(json.dumps(data), content_type="application/json")


def project_stats_api(request, project):
    start = time.time()
    data = {"stats": get_project_stats(project)}
    data["loadtime"] = time.time() - start
    return HttpResponse(json.dumps(data), content_type="application/json")
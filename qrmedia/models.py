from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator
from django.db.models import Sum
from pywiki_light import *
import re
import hashlib
from urllib.parse import quote
from SPARQLWrapper import SPARQLWrapper, JSON
from partial_date import PartialDateField

# Validators
validate_qid = RegexValidator(r"^Q[1-9][0-9]*$")
validate_project = RegexValidator(r"^[a-z-]+$")
validate_month = RegexValidator(r"^[1-2]\d{3}\-(1[0-2]|0[1-9])$")


class Project(models.Model):
    name = models.CharField(max_length=200, unique=True, validators=[validate_project])
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Item(models.Model):
    """
    A class to get labels for items saved in the DB without having to
    query Wikidata each time
    """

    qid = models.CharField(max_length=20, unique=True, validators=[validate_qid])
    label = models.CharField(max_length=200, blank=True, default="")
    created_date = models.DateTimeField(default=timezone.now)

    def save(self, lang="en", *args, **kwargs):
        if self.label=="":
            article = Article(self.qid)
            article.get_live_wd_data()
            self.label = article.wd_label
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.label} ({self.qid})"


class Panel(models.Model):
    qid = models.ForeignKey("Item", on_delete=models.CASCADE, related_name="panel_qid")
    project = models.ForeignKey("Project", on_delete=models.SET_NULL, null=True)
    place = models.ForeignKey(
        "Item", on_delete=models.CASCADE, null=True, related_name="panel_place"
    )
    created_date = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ("qid", "project", "place")

    def __str__(self):
        return f"{self.qid} / {self.project} / {self.place}"


class Stat(models.Model):
    panel = models.ForeignKey("Panel", on_delete=models.CASCADE, unique_for_month="month")
    month = models.DateField()
    counter = models.PositiveIntegerField(default=0)

    def collect(self):
        print(self.panel.qid, self.panel.project.name, self.panel.place, self.month)

    def __str__(self):
        return f"{self.month} - {self.panel}"


class Article:
    """
    Called in the /api/item view.
    """

    def __init__(self, qid, lang="en"):
        self.qid = qid
        self.lang = lang

        self.commons_category = ""
        self.commons_sitelink = ""
        self.image = ""
        self.type = ""
        self.wd_claims = {}
        self.wd_sitelinks = {}
        self.wd_sitelinks_number = 0
        self.wd_description = ""
        self.wd_label = ""
        self.wb_sitelink = ""
        self.wn_sitelink = ""
        self.wp_sitelink = ""
        self.wp_summary = ""
        self.wp_title = ""
        self.wq_sitelink = ""
        self.ws_sitelink = ""
        self.ws_works = False
        self.wspec_sitelink = ""
        self.wver_sitelink = ""
        self.wvoy_banner = ""
        self.wvoy_sitelink = ""
        self.pronunciation = ""

    def get_data(self):
        return {
            "qid": self.qid,
            "lang": self.lang,
            "type": self.type,
            # 'wd_claims': self.wd_claims,
            "commons_sitelink": self.commons_sitelink,
            "commons_category": self.commons_category,
            "image": self.image,
            "pronunciation": self.pronunciation,
            # 'wd_sitelinks': self.wd_sitelinks,
            "wd_description": self.wd_description,
            "wd_label": self.wd_label,
            "wb_sitelink": self.wb_sitelink,
            "wn_sitelink": self.wn_sitelink,
            "wp_sitelink": self.wp_sitelink,
            "wp_summary": self.wp_summary,
            "wq_sitelink": self.wq_sitelink,
            "ws_sitelink": self.ws_sitelink,
            "ws_works": self.ws_works,
            "wspec_sitelink": self.wspec_sitelink,
            "wver_sitelink": self.wver_sitelink,
            "wvoy_banner": self.wvoy_banner,
            "wvoy_sitelink": self.wvoy_sitelink,
        }

    def get_live_wd_data(self, lang=""):
        if not lang:
            lang = self.lang
        try:
            wdwiki = Pywiki("wikidatawiki")
            wdwiki.login()

            props = [
                "info",
                "aliases",
                "labels",
                "descriptions",
                "claims",
                "datatype",
                "sitelinks/urls",
            ]

            payload = {
                "action": "wbgetentities",
                "format": "json",
                "ids": self.qid,
                "props": "|".join(props),
                "languages": lang,
                "languagefallback": 1,
                "formatversion": "2",
            }

            results = wdwiki.request(payload)
            entity = results["entities"][self.qid]

            # claims
            if len(entity["claims"]):
                self.wd_claims = entity["claims"]

                # P31 : Instance of
                if "P31" in self.wd_claims:
                    item_type = get_value_from_statements(
                        self.wd_claims["P31"], "first"
                    )
                    if item_type == "Q5":
                        self.type = "human"
                    elif item_type in [
                        "Q484170",
                        "Q2989454",
                        "Q20899166",
                        "Q22927548",
                        "Q22927616",
                    ]:
                        self.item_type = "commune française"

                # P373: Commons category

                if "P373" in self.wd_claims:
                    self.commons_category = get_value_from_statements(
                        self.wd_claims["P373"], "first"
                    )

                # P18: (main) image
                if "P18" in self.wd_claims:
                    self.image = commons_file_url(
                        sanitize_file_name(
                            get_value_from_statements(self.wd_claims["P18"], "first")
                        ),
                        800,
                    )

                # P443: Pronunciation
                # TODO I18n
                if "P443" in self.wd_claims:
                    lang_qid = get_language_qid(self.lang)
                    filename = get_value_from_statements(
                        self.wd_claims["P443"], "qualifiers", [("P407", lang_qid)]
                    )
                    if filename:
                        target = commons_file_url(sanitize_file_name(filename))
                        ext = filename.split(".")[-1].lower()
                        if ext == "mp3":
                            type = "audio/mpeg"
                        elif ext in ["oga", "ogg"]:
                            type = "audio/ogg"
                        else:
                            type = "audio/wav"

                        self.pronunciation = {"target": target, "type": type}

                # P800: Notable works
                if "P800" in self.wd_claims:
                    self.ws_works = True

                # P948: Wikivoyage banner
                if "P948" in self.wd_claims:
                    filename = get_value_from_statements(
                        self.wd_claims["P948"], "first"
                    )
                    target = "https://commons.wikimedia.org/wiki/File:{}".format(
                        filename
                    )
                    thumb = commons_file_url(sanitize_file_name(filename), 600,)
                    self.wvoy_banner = {"target": target, "thumb": thumb}
                # """

            # descriptions
            if lang in entity["descriptions"]:
                self.wd_description = entity["descriptions"][lang]["value"]

            # labels
            if lang in entity["labels"]:
                self.wd_label = entity["labels"][lang]["value"]

            # sitelinks
            self.wd_sitelinks_number = len(entity["sitelinks"])
            self.wd_sitelinks = entity["sitelinks"]

            if self.wd_sitelinks_number:
                wpdb = lang + "wiki"
                wbdb = lang + "wikibooks"
                wndb = lang + "wikinews"
                wqdb = lang + "wikiquote"
                wsdb = lang + "wikisource"
                wverdb = lang + "wikiversity"
                wvoydb = lang + "wikivoyage"

                if wpdb in entity["sitelinks"]:
                    self.wp_sitelink = entity["sitelinks"][wpdb]["url"]
                    if self.wp_title == "":
                        self.wp_title = entity["sitelinks"][wpdb]["title"]

                if wbdb in entity["sitelinks"]:
                    self.wb_sitelink = entity["sitelinks"][wbdb]["url"]

                if wndb in entity["sitelinks"]:
                    self.wn_sitelink = entity["sitelinks"][wndb]["url"]

                if wqdb in entity["sitelinks"]:
                    self.wq_sitelink = entity["sitelinks"][wqdb]["url"]

                if wsdb in entity["sitelinks"]:
                    self.ws_sitelink = entity["sitelinks"][wsdb]["url"]

                if wverdb in entity["sitelinks"]:
                    self.wver_sitelink = entity["sitelinks"][wverdb]["url"]

                if wvoydb in entity["sitelinks"]:
                    self.wvoy_sitelink = entity["sitelinks"][wvoydb]["url"]

                if "specieswiki" in entity["sitelinks"]:
                    self.wspec_sitelink = entity["sitelinks"]["specieswiki"]["url"]

                if "commonswiki" in entity["sitelinks"]:
                    self.commons_sitelink = entity["sitelinks"]["commonswiki"]["url"]
                elif self.commons_category:
                    self.commons_sitelink = self.commons_category

            wdwiki.close()

        except Exception as e:
            print("Can't retrieve live WD data for {}: {}".format(self.qid, e))

    def get_wp_summary(self, lang=""):
        if not lang:
            lang = self.lang
        if self.wp_title:
            restapi_url = "https://{}.wikipedia.org/api/rest_v1/".format(lang)
            wp_title = re.sub(" ", "_", self.wp_title)
            payload = "page/summary/{}".format(wp_title)
            data = requests.get(restapi_url + payload).json()

            if "extract_html" in data:
                self.wp_summary = data["extract_html"]


def get_works(qid, lang="en", limit=3):
    """
    Called in the /api/works view.
    """
    if isinstance(limit, str) and limit.isnumeric():
        limit = int(limit)
    else:
        limit = 3
    works = []
    results = wikidata_sparql_query(
        """
        SELECT ?title ?sitelink
        WHERE
        {{
          ?work wdt:P50 wd:{} .
          ?edition wdt:P629 ?work .
          ?edition wdt:P31 wd:Q3331189 .
          ?edition wdt:P1476 ?title .
          ?sitelink schema:about ?edition .
          ?sitelink schema:inLanguage "{}" .
        }} LIMIT {}""".format(
            qid, lang, limit
        )
    )

    for r in results["results"]["bindings"]:
        works.append({"title": r["title"]["value"], "sitelink": r["sitelink"]["value"]})

    return works


def get_commons_files(category, number=30, thumb_size=100):
    """
    Called in the /api/commons_files view.
    """
    if isinstance(number, str) and number.isnumeric():
        number = int(number)
    elif isinstance(number, int):
        pass
    else:
        number = 30

    if isinstance(thumb_size, str) and thumb_size.isnumeric():
        thumb_size = int(thumb_size)
    elif isinstance(thumb_size, int):
        pass
    else:
        thumb_size = 80
    ps_base_url = "https://petscan.wmflabs.org/"
    payload = {
        "language": "commons",
        "project": "wikimedia",
        "depth": 2,
        "categories": category,
        "ns[6]": 1,
        "format": "json",
        "sortby": "random",
        "doit": "Do%20it",
    }
    data = requests.get(ps_base_url, params=payload).json()
    files = data["*"][0]["a"]["*"]

    if len(files):
        """
        Per list on https://commons.wikimedia.org/wiki/Commons:File_types 
        only these filetypes can generate a proper thumb in all cases
        """
        allowed_exts = ["jpe", "jpg", "jpeg", "gif", "png"]

        output = []

        for f in files:
            ext = f["title"].split(".")[-1].lower()
            if ext in allowed_exts and len(output) < number:
                target = "https://commons.wikimedia.org/wiki/File:{}".format(f["title"])
                thumb = commons_file_url(sanitize_file_name(f["title"]), thumb_size)
                output.append({"target": target, "thumb": thumb, "title": f["title"]})

    return {"files": output, "total": len(files)}


def sanitize_file_name(filename):
    if filename[:8] == "Fichier:":
        filename = filename[8:]
    elif filename[:5] == "File:":
        filename = filename[5:]

    filename = re.sub(" ", "_", filename)

    return filename


def commons_file_url(filename, width=0):
    # Returns the direct URL of a file on Wikimedia Commons.
    # Per https://frama.link/commons_path

    hashed_filename = hashlib.md5(filename.encode("utf-8")).hexdigest()

    base_url = "https://upload.wikimedia.org/wikipedia/commons"

    if not width:
        path = "{}/{}/{}".format(hashed_filename[:1], hashed_filename[:2], filename)

    else:
        path = "thumb/{}/{}/{}/{}px-{}".format(
            hashed_filename[:1], hashed_filename[:2], filename, width, filename
        )
        if filename[-4:].lower() == ".svg":
            path += ".png"

    return "{}/{}".format(base_url, quote(path))


def get_value_from_statements(statements, sorting="all", qualifiers=[]):
    # Given a list of WD statements, get one value
    # or a list of values
    if sorting == "all":
        values = []
        for s in statements:
            values.append[get_value_from_statement(s)]
        return values
    elif sorting == "first":
        value = get_value_from_statement(statements[0])
        return value
    elif sorting == "newest":
        date_props = ["P580", "P585"]
        most_recent_val = 0
        most_recent_year = 0
        for s in statements:
            for prop in date_props:
                if "qualifiers" in s and prop in s["qualifiers"]:
                    year = int(
                        extract_year(s["qualifiers"][prop][0]["datavalue"]["value"])
                    )
                    if year > most_recent_year:
                        most_recent_year = year
                        most_recent_val = get_value_from_statement(s)
        if most_recent_year == 0:
            # If no date qualifier found, use the first available
            most_recent_val = get_value_from_statements(statements, "first")
        return most_recent_val
        pass
    elif sorting == "qualifiers":
        for s in statements:
            for q in qualifiers:
                prop, qval_source = q
                if "qualifiers" in s and prop in s["qualifiers"]:
                    qval_target = s["qualifiers"][prop][0]["datavalue"]["value"]["id"]
                    if qval_source == qval_target:
                        return get_value_from_statement(s)
        # If no perfect match, return None
        return None


def get_value_from_statement(statement):
    base_value = statement["mainsnak"]["datavalue"]["value"]
    if statement["mainsnak"]["datatype"] == "wikibase-item":
        value = base_value["id"]
    else:
        value = base_value

    return value


def extract_year(value):
    # Returns the date from a WD date value
    year_regex = r"^(?P<year>[+-]*\d+)-"
    return re.match(year_regex, value["time"]).group("year")


def get_language_qid(lang):
    languages_qid = {
        "ar": "Q13955",
        "br": "Q12107",
        "cat": "Q7026",
        "de": "Q188",
        "en": "Q1860",
        "es": "Q1321",
        "fr": "Q150",
        "it": "Q652",
        "pt": "Q5146",
        "ru": "Q7737",
    }

    if lang in languages_qid:
        return languages_qid[lang]


def wikidata_sparql_query(query):
    """
    Queries WDQS and returns the result
    """
    endpoint = "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
    sparql = SPARQLWrapper(endpoint, agent="QRMedia tool for Wikidata")
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results


def get_panel_stats(panel_id):
    panels = Panel.objects.filter(id=panel_id)
    if len(panels) == 1:
        data = {}
        panel = panels[0]
        data['panel'] = {
            'item_qid': panel.qid.qid,
            'item_label': panel.qid.label,
            'project': panel.project.name,
        }
        if panel.place:
            data['panel']['place_qid'] = panel.place.qid
            data['panel']['place_label'] = panel.place.label

        stats = Stat.objects.filter(panel=panel).order_by('month')
        results = []
        for s in stats:
            results.append({
                'month': s.month.strftime('%Y-%m'), 'visits': s.counter})
        data["stats"] = results
        return data
    elif len(panels) == 0:
        return {'error': 'Panel not found'}
    else:
        return {'error': 'Could not identify panel'}


def get_project_stats(project, limit=None):
    query = Stat.objects.filter(
        panel__project__name=project).values_list(
        'panel_id',
        'panel__qid__qid',
        'panel__qid__label',
        'panel__place__qid',
        'panel__place__label').annotate(total_count=Sum('counter')).order_by('-total_count')

    if len(query):
        most_viewed = []
        rank = 1
        for q in query:
            most_viewed.append({
                'panel_id': q[0],
                'item_qid': q[1],
                'item_label': q[2],
                'place_qid': q[3],
                'place_label': q[4],
                'visits': q[5],
                'rank': rank})
            rank += 1
            if limit and rank == limit:
                break
        return({'most_viewed': most_viewed})
    else:
        return {'error': 'No data for this panel/month'}

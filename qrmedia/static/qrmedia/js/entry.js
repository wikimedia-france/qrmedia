var getCommonsFiles = function( commons_category ) {
	var commons_gallery = new Vue({
	  el: '#commons-gallery',
	  data: {
	    gallery: "",
	    seen: false
	  }
	});

	$.get('/api/commons_files/' + commons_category, {'number': 6, 'thumb': 80}, function( data ){
		var output_files = [];
		if ('files' in data) {
			var files = data['files'];
			

			for (var i=0;i<files.length;i++) {
				output_files.push({'target':files[i]['target'], 'thumb': files[i]['thumb']});
			}
			commons_gallery.gallery = output_files;
			commons_gallery.seen = true;
		}		
	});
};

var getWorks = function ( ws_works, works_list ) {

	$.get('/api/works/' + ws_works, { 'limit': 3 }, function ( data ) {
		var output_works = [];

		if (('works' in data) && (data['works'].length>=1)) {
			var works = data['works'];
			console.log[works];

			for (var i=0;i<works.length;i++) {
				var title = works[i]['title'];
				if (title.length > 35) {
					title = title.substring(0,34) + '…';
				}
				output_works.push({'title': title, 'sitelink': works[i]['sitelink']});
			}

			works_list.works = output_works;
			works_list.seen = true;
			console.log(works_list);
		}
	});
}


var loadData = function(payload) {
	if ($('#works-list').length) {
		var works_list = new Vue({
		  el: '#works-list',
		  data: {
		    works: "",
		    seen: false
		  }
		});
	}
	

	if ('commons_category' in payload) {
		getCommonsFiles(payload['commons_category']);		
	}
	if ('ws_works' in payload) {
		getWorks(payload['ws_works'], works_list);
	}
};

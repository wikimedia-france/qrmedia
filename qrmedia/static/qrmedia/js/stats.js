var statsProjectVue = new Vue({
  el: '#stats-project-app',
  data: {
    projectName: "",
    panels: [],
    seen: false
  },
  methods: {
    updateProjectPanels: function(project) {
        this.projectName = project;
        getProjectPanels(project);
    }
  },
});

var statsPanelVue = new Vue({
  el: '#stats-panel-app',
  data: {
    projectName: "",
    panelName: "",
    seen: false
  },
  methods: {
    updatePanelStats: function(panel) {
        this.panelName = panel;
        getPanelStats(panel);
    }
  },
});

var getProjectPanels = function (project, target) {
    $.get('/api/stats/project/' + project, {'a': 'b'}, function( data ){
        var panelRows = [];
        if ('stats' in data) {
            if ('most_viewed' in data['stats']) {
                var panels = data['stats']['most_viewed'];
                for (var i=0;i<panels.length;i++) {
                    var panel_id = panels[i]['panel_id'];
                    var panel_name = panels[i]['item_label'] + " (" + panels[i]['item_qid'] + ")";
                    if (panels[i]['place_qid']) {
                        panel_name += " / " + panels[i]['place_label'];
                    }
                    var panel_url = "/stats/" + project + "/" + panel_id;
                    var visits = panels[i]['visits'];
                    var rank = panels[i]['rank'];
                    panelRows.push({
                        'url': panel_url,
                        'name': panel_name,
                        'visits': visits,
                        'rank': rank
                    });
                }
            }
        }
        statsProjectVue.panels = panelRows;
    });
};

var getPanelStats = function (panel) {
    $.get('/api/stats/panel/' + panel, {'a': 'b'}, function( data ){
        var panelRows = [];
        if ('stats' in data) {
            console.log(data);
        }
    });
};


$( document ).ready(function(){
    /* Scripts for the project stats page */
    if ($('#stats-project-app').length) {
        var project = $('#form-project option:selected').text().trim();
        statsProjectVue.updateProjectPanels(project);
        statsProjectVue.seen = true;

        $('#form-project').on('change', function() {
            if (this.value) {
                statsProjectVue.updateProjectPanels(this.value);
                statsProjectVue.seen = true;
            } else {
                statsProjectVue.seen = false;
            }
        });
    } /* Scripts for the panel stats page */
    else if ($('#stats-panel-app').length) {
        var path = window.location.pathname.split('/');
        var project = path[2];
        var panel = path[3];
        console.log(path);
        statsPanelVue.updatePanelStats(panel);
        statsPanelVue.seen = true;
    }
});
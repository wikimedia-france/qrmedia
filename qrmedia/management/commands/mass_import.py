#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError
from qrmedia.models import Item, Project, Panel
from qrmedia.models import validate_qid, validate_project
import csv
import os
import qrcode
import qrcode.image.svg


class Command(BaseCommand):
    help = "Import panels to the database for tracking"

    def add_arguments(self, parser):
        parser.add_argument("input_file", nargs=1, type=str)
        parser.add_argument(
            "--generate",
            type=str,
            help="Generates the qrcodes in the format specified as value (png or svg)",
        )

    def handle(self, *args, **options):
        if options["generate"]:
            if options["generate"].lower() == "svg":
                gen_type = "svg"
            elif options["generate"].lower() == "png":
                gen_type = "png"
            else:
                raise ValidationError(
                    f"invalid parameter for generate: {options['generate']}"
                )
        else:
            gen_type = None

        input_file = os.path.join("resources", options["input_file"][0])

        with open(input_file, "r") as inputs:
            reader = csv.DictReader(inputs)
            for row in reader:
                if validate_qid(row["qid"]) is None:
                    qid, return_code = Item.objects.get_or_create(qid=row["qid"])

                if row["campaign"] and (validate_project(row["campaign"]) is None):
                    project, return_code = Project.objects.get_or_create(
                        name=row["campaign"]
                    )
                else:
                    project = None

                if row["kwd"] and (validate_qid(row["kwd"]) is None):
                    place, return_code = Item.objects.get_or_create(qid=row["kwd"])
                else:
                    place = None

                panel = Panel.objects.get_or_create(
                    qid=qid, project=project, place=place
                )
                print(panel)

                if gen_type:
                    url = f"https://qrwm.org/{qid.qid}"
                    name = qid.label
                    arguments = []
                    if project:
                        arguments.append(f"pk_campaign={project.name}")
                        name = f"{name} - {project.name}"
                    if place:
                        arguments.append(f"pk_kwd={place.qid}")
                        name = f"{name} - {place.label}"
                    url_args = "&".join(arguments)

                    if url_args:
                        url = f"{url}?{url_args}"

                    generate_code({"url": url, "name": name, "format": gen_type})


def generate_code(data):
    qr = qrcode.QRCode(
        version=4,
        error_correction=qrcode.constants.ERROR_CORRECT_Q,
        box_size=10,
        border=4,
    )
    qr.add_data(data["url"])
    qr.make(fit=True)

    file_format = data["format"]
    if file_format == "png":
        img = qr.make_image(fill_color="black", back_color="white")
    if file_format == "svg":
        factory = qrcode.image.svg.SvgPathFillImage
        img = qrcode.make(data["url"], image_factory=factory)

    filename = data["name"]

    imagepath = os.path.join("resources", "outputs", f"{filename}.{file_format}")
    img.save(imagepath)

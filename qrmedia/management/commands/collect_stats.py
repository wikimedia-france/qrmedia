#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError
from qrmedia.models import Item, Project, Panel, Stat
from qrmedia.models import validate_month, validate_project
from os import path
from datetime import date, timedelta
import configparser
import requests

config = configparser.ConfigParser()
config.read("config.ini")

MATOMO_TOKEN = config.get("matomo", "token")
MATOMO_HOST = config.get("matomo", "host")
MATOMO_SITE = config.get("matomo", "site")


class Command(BaseCommand):
    help = "Collect panels visits to the database for tracking"

    def add_arguments(self, parser):
        parser.add_argument(
            "--project",
            type=str,
            help="A project tag (if not provided, the command will be run on all of them)",
        )
        parser.add_argument(
            "--month",
            type=str,
            help="A project tag (if not provided, the command will be run on the previous month)",
        )

    def handle(self, *args, **options):
        if options["project"]:
            try:
                if validate_project(options["project"]) is None:
                    panels = Panel.objects.filter(project__name=options["project"])
            except:
                raise ValidationError(
                    f"invalid parameter for project: {options['project']}"
                )
        else:
            panels = Panel.objects.all()

        if options["month"]:
            try:
                if validate_month(options["month"]) is None:
                    month = options["month"]
            except:
                raise ValidationError(
                    f"invalid parameter for month: {options['month']}"
                )
        else:
            last_month = date.today().replace(day=1) - timedelta(
                days=1
            )  # last day of last month
            month = last_month.strftime("%Y-%m")

        for p in panels:
            get_panel_stat(p, month)


def get_panel_stat(panel, month):
    print(f"Getting data for panel {panel} in {month}:")
    base_url = MATOMO_HOST
    payload = {
        "module": "API",
        "method": "Actions.getPageUrl",
        "idSite": MATOMO_SITE,
        "pageUrl": panel.qid.qid,
        "period": "month",
        "date": month,
        "format": "JSON",
        "filter_limit": 10,
        "token_auth": MATOMO_TOKEN,
    }

    segment = []
    if panel.project:
        segment.append(f"referrerType==campaign;referrerName=={panel.project.name}")

    if panel.place:
        segment.append(f"referrerKeyword=={panel.place.qid}")

    if segment:
        payload["segment"] = (";").join(segment)

    r = requests.get(base_url, params=payload)
    result = r.json()
    if len(result):
        counter = result[0]["nb_visits"]
    else:
        counter = 0

    month_day = f"{month}-01"
    stat = Stat.objects.get_or_create(panel=panel, month=month_day, counter=counter)
    print(stat, counter)

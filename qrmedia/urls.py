from django.conf.urls import url
from django.urls import path, re_path

from . import views

urlpatterns = [
    url(r"^api/item/(?P<qid>Q[1-9][0-9]*)$", views.item_api, name="item_api"),
    url(r"^api/works/(?P<qid>Q[1-9][0-9]*)$", views.works_api, name="works_api"),
    url(
        r"^api/commons_files/(?P<category>[^\/]*)$",
        views.commons_files_api,
        name="commons_files_api"),
    url(
        r"^api/stats/panel/(?P<panel_id>\d+)?$",
        views.panel_stats_api,
        name="panel_stats_api"),
    url(
        r"^api/stats/project/(?P<project>[a-z-]+)$",
        views.project_stats_api,
        name="project_stats_api"),
    url(
        r"^stats/(?P<project>[a-z-]+)?(\/)?$",
        views.project_stats,
        name="project_stats"),
    url(
        r"^stats/(?P<project>[a-z-]+)?\/(?P<panel>\d+)$",
        views.panel_stats,
        name="panel_stats"),
    path("", views.index, name="index"),
    re_path(r"^(?P<qid>Q[1-9][0-9]*)$", views.portal, name="portal"),
]

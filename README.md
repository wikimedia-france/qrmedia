# QRmedia

**QRmedia** is a mobile-friendly tool which uses QR codes to deliver access to Wikimedia content to users in their preferred language.

It is similar to QRpedia, but instead of being Wikipedia-centric, it aims at featuring the contents we have on all Wikimedia projects.
Also, by relying on the Wikidata item QID instead of the Wikipedia article title, the URLs (and so the QR codes) are more stable in time.

Try it out at https://qrwm.org/ !
More info at https://meta.wikimedia.org/wiki/QRmedia

Some examples of landing pages:
- Person (Louise Michel) https://qrwm.org/Q216092
- Plant species (baobab) https://qrwm.org/Q158742
- Animal species (hermine) https://qrwm.org/Q25345
- Country (Madagascar) https://qrwm.org/Q1019
- City (Montréal) https://qrwm.org/Q340
- Event (Holi) https://qrwm.org/Q10259

## Install
### Packages
- `apt install libmysqlclient-dev libffi-dev libssl-dev libssl-doc zlib1g-dev python3-dev python3-venv`

### Prepare database
- Connect to MySQL and create a database for the tool:
 
- `CREATE DATABASE qrmedia;`
- `CREATE USER 'qrmedia'@'localhost' IDENTIFIED BY 'password';`
- `GRANT ALL PRIVILEGES ON qrmedia.* TO 'qrmedia'@'localhost';`
- ```ALTER DATABASE `qrmedia` CHARACTER SET utf8```; 
- `FLUSH PRIVILEGES;`

### Install the project

- `git clone` this repository somewhere and `cd` in.
- `cp config.ini.sample config.ini`
- Fill the config.ini file
- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip install wheel`
- `pip install -r requirements.txt`
- `python3 manage.py migrate`
- `python3 manage.py collectstatic`

### Launch it
 - `python3 manage.py runserver`


 ## Translations
 Translations are managed collaboratively through Translatewiki.net: https://translatewiki.net/wiki/Special:Translate/qrmedia
 
 ### Update translations templates
 - `python manage.py makemessages --keep-pot --ignore venv -l de -l fr -l ca -l es -l it -l sv -l ar`
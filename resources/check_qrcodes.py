#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import listdir, path
import argparse
import qrtools
import re
import csv

pattern = r"https:\/\/qrwm.org\/(?P<qid>Q[0-9]+)(\?)*(pk_campaign=(?P<campaign>[a-z]+))*(&)*(pk_kwd=(?P<kwd>Q[0-9]+))*"
prog = re.compile(pattern)

fieldnames = ["qid", "campaign", "kwd"]


def read_code(filename):
    qr = qrtools.QR()
    qr.decode(filename)
    return qr.data


def parse_dir(directory):
    files = listdir(directory)
    all_codes = []
    for f in files:
        if f.endswith(".png"):
            try:
                target = read_code(path.join(directory, f))
                result = prog.match(target)
                parts = {
                    "qid": result.group("qid"),
                    "campaign": result.group("campaign"),
                    "kwd": result.group("kwd"),
                }
                all_codes.append(parts)

            except AttributeError:
                if target == "NULL":
                    print(f"File {f} does not seem to be a valid QR code")
                else:
                    print(
                        f"File {f} does not seem to match the pattern and contains {target}"
                    )
    return all_codes


def write_output(output_file, codes_list):
    with open(output_file, "w") as csv_output:
        writer = csv.DictWriter(csv_output, fieldnames=fieldnames)
        writer.writeheader()
        for code in codes_list:
            writer.writerow(code)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Parse a folder containing PNGs of QRmedia codes and returns a CSV of the targets."
    )
    argparser.add_argument(
        "folder", help="The name of the folder to parse",
    )

    args = argparser.parse_args()

    target_dir = args.folder

    if target_dir:
        print(f"Parsing QR codes in folder {target_dir}")
        all_codes = parse_dir(target_dir)
        output_file = f"{target_dir}.csv"
        write_output(output_file, all_codes)
